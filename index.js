// console.log("Hello World!");

// Add Two Numbers
function addTwoNumbers(firstNumber, secondNumber) {

	let sum = firstNumber + secondNumber;
	console.log("Displayed sum of " + firstNumber + " and " + secondNumber);
	console.log(sum);
}
addTwoNumbers(100,10);



// Subtract Two Numbers
function subtractTwoNumbers (thirdNumber, fourthNumber) {

	let difference = thirdNumber - fourthNumber;
	console.log("Displayed difference of " + thirdNumber + " and " + fourthNumber);
	console.log(difference);
}

subtractTwoNumbers(789,456);

// Multiply Two Numbers
function multiplyTwoNumbers(numFive, numSix) {

	return numFive*numSix;
};

// Divide Two Numbers
function divideTwoNumbers(numSeven, numEight) {
	return numSeven/numEight;
};

let product = multiplyTwoNumbers(123,321);
let quotient = divideTwoNumbers(55,11);

console.log("The product of 123 and 321:");
console.log(product);

console.log("The quotient of 55 and 11");
console.log(quotient);


// Area of Circle
function calculateArea(radius) {

	return Math.PI*(radius**2);
}

let areaCircle = calculateArea(5);

console.log("The result of getting the area of a circle with 5 radius: ");
console.log(areaCircle);

// Average of Four Numbers

function calculateAverage(numberOne, numberTwo, numberThree, numberFour) {

	return  (numberOne + numberTwo + numberThree + numberFour)/4;
	
}

let getAverage = calculateAverage(77,75,79,78);
console.log("The result of 77,75,79 and 78:");
console.log(getAverage);

// Passing Score

function getPassing(myScore, totalScore) {

	return(myScore/totalScore)*100 > 75;
}

let isPassingScore = getPassing(1,9);
console.log("Is 1/9 a passing score?");
console.log(isPassingScore);







